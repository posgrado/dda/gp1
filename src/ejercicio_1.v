`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 19.09.2021 16:29:51
// Design Name:
// Module Name: ejercicio_1
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ejercicio_1(
    input       [2:0]   i_data1,
    input       [2:0]   i_data2,
    input       [1:0]   i_sel,
    input               i_reset_n,
    input               clk,
    output reg  [5:0]   o_data,
    output reg          o_overflow
  );
  reg   [3:0]   sum1;
  reg   [5:0]   mux;
  reg   [6:0]   sum2;
  always @(*)
  begin
    sum1 = i_data1 + i_data2;
    case (i_sel)
      2'b00:
        mux <= {1'b0, i_data1};
      2'b01:
        mux <= {1'b0, i_data2};
      2'b10:
        mux <= sum1;
      2'b11:
        mux <= 0;
    endcase
    sum2 = mux + o_data;
  end
  always @(posedge clk or negedge i_reset_n)
  begin
    if(i_reset_n==1'b0)
      {o_overflow, o_data} <= 7'b000_0000;
    else
      {o_overflow, o_data} <= sum2;
  end
endmodule
