`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 19.09.2021 16:26:44
// Design Name:
// Module Name: ejercicio_1_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ejercicio_1_tb();

  reg       [2:0]    i_data2;
  reg       [2:0]    i_data1;
  reg       [1:0]    i_sel;
  reg                i_reset_n;
  reg                 clk;
  wire        [5:0]    o_data;
  wire                 o_overflow;

  //! Stimulus by initial
  initial
  begin: stimulus
    i_data1[2:0]        = 3'b000    ;
    i_data2[2:0]        = 3'b000    ;
    i_sel[1:0]          = 2'b00     ;
    clk                 = 1'b0      ;
    i_reset_n           = 1'b0      ;
    #100 i_reset_n      = 1'b1      ;
    #200 i_data1[2:0]   = 3'b001    ;
    #600 i_data1[2:0]   = 3'b000    ;
    #1000 i_data2[2:0]   = 3'b001    ;
    #1300 i_sel[1:0]   = 2'b01    ;

    #3000 $finish;
  end

  //! Clock generator
  always #5 clk = ~clk;

  ejercicio_1
    u_ejercicio_1(
      .i_data1    (i_data1)   ,
      .i_data2    (i_data2)   ,
      .i_sel      (i_sel)     ,
      .i_reset_n  (i_reset_n) ,
      .clk        (clk)       ,
      .o_data     (o_data)    ,
      .o_overflow  (o_overflow)
    );
endmodule
